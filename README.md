# ru-wiki-tables-dataset



A corpus of tables containing Russian language tables extracted from Wikipedia. It is collected using the RWT (Russian Web Tables) toolkit, which can be found in the following repositories:
1) https://gitlab.com/unidata-labs/ru-wiki-tables-backend
2) https://gitlab.com/unidata-labs/ru-wiki-tables-frontend

If you are going to use the corpus or the toolkit, please cite the following paper:
Platon Fedorov, Alexey Mironov, and George Chernishev. Russian Web Tables: A Public Corpus of Web Tables for Russian Language Based on Wikipedia. DAMDID'22 (accepted).
